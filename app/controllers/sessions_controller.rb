class SessionsController < ApplicationController
    def new
    end

    def create
        @user = User.find_by(email: params[:email])
        if @user && @user.authenticate(params[:password])
            session[:user_id] = @user.id
            redirect_to root_path, notice: "Logged in successfully!"
            # render json: @user
        else
            flash[:alert] = "Invalid email or password"
            render :new
            # render json: @user
        end
    end 

    def destroy
        session[:user_id] = nil
        redirect_to root_path, notice: "Logged out"
    end
end